import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer _min = null;

        for (int i = 0; i < liste.size(); ++i)
        {
            if (_min == null || liste.get(i) < _min) _min = liste.get(i);
        }

        return _min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean plusPetit = true;
        int i = 0;

        while (plusPetit && i < liste.size())
        {
            if (liste.get(i).compareTo(valeur) <= 0) plusPetit = false;
            ++i;
        }

        return plusPetit;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listeInter = new ArrayList<>();
        int i1 = 0;
        int i2 = 0;

        while (i1 < liste1.size() && i2 < liste2.size())
        {
            if (liste1.get(i1).compareTo(liste2.get(i2)) > 0) ++i2;
            else if (liste1.get(i1).compareTo(liste2.get(i2)) < 0) ++i1;
            else
            {
                if (!listeInter.contains(liste1.get(i1))) listeInter.add(liste1.get(i1));
                ++i1;
                ++i2;
            }
        }

        return listeInter;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        return null;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

        return null;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int nbParOuv = 0;
        int nbParFer = 0;
        boolean debutParFer = false;

        for (Character c : chaine.toCharArray())
        {
            if (c.equals('(')) ++nbParOuv;
            else 
            {
                if (nbParOuv == nbParFer) debutParFer = true;
                ++nbParFer;
            }
        }

        return ((nbParOuv == nbParFer) && !debutParFer);
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        return true;
    }



}
